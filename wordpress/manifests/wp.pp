class wordpress::wp inherits wordpress
{
archive { '/tmp/latest.zip':
  source       => 'https://wordpress.org/latest.zip',
  extract      => true,
  extract_path => '/var/www/html/',
  creates      => '/var/www/html/wordpress',
  cleanup      => false,
  before => File['/var/www/html/wordpress/wp-config.php'],
}

#file{"/var/www/html/wordpress/wp-config.php":
#    ensure =>present,
#    source => "/tmp/wp-config-sample.php",
#}

file{"/var/www/html/wordpress/wp-config.php":
    ensure =>present,
    content => template('wordpress/wp-config-sample.php.erb'),
}

file {"change file mode":
  path => "/var/www/html/wordpress",
  mode => "0775",
  owner => "www-data",
  require => Archive['/tmp/latest.zip'],
  }

 
}
